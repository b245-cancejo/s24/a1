//console.log("hello")

let number = 3
let getCube = Math.pow(number,3);
console.log(`The cube of ${number} is ${getCube}`);

let address = [123, "Ipil-ipil St.", "Brgy. San Jose", "Baggao", "Tuguegarao", "Cagayan", 3509];
const [hNumber, street, barangay, municipality, province, zipCode] = address;
console.log(`I live at ${hNumber}, ${street}, ${barangay}, ${municipality}, ${province}, ${zipCode}`);

let animal = {
	name: "Mirana",
	species: "Dog",
	weight: "20 kgs",
	measurement: "5 ft 2 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`)

let numbers = [2,4,6,8,10];

const numberArray = numbers.forEach((number) => console.log(number))

let reduceNumber = numbers.reduce((x, y) =>(x+y));
console.log("Using reduce iteration in arrow function: ");
console.log(reduceNumber);	

class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}
let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);